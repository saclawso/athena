# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

PrimaryVerticesCPContentRun4 = [
"PrimaryVertices",
"PrimaryVerticesAux.time.timeResolution.hasValidTime"
]
